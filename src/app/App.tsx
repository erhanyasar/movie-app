import { Box, Container } from "@mui/material";
import Router from "./router";
import "../assets/styles.scss";

export default function App(): JSX.Element {
  return (
    <Container maxWidth="xl" className="containerWrapper">
      <Box className="boxWrapper">
        <Router />
      </Box>
    </Container>
  );
}
