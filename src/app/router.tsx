import { BrowserRouter, Routes, Route } from "react-router-dom";
import MovieWrapper from "../movieWrapper/movieWrapper";
import MovieDetails from "../movieDetails/movieDetails";

function Router(): JSX.Element {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<MovieWrapper />} />
        <Route path="/movie-details/:id" element={<MovieDetails />} />
      </Routes>
    </BrowserRouter>
  );
}

export default Router;
