import { configureStore } from "@reduxjs/toolkit";
import movieReducer from "../movieWrapper/movieWrapperSlice";

export const store = configureStore({
  reducer: {
    movies: movieReducer,
  },
});
