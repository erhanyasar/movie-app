export const baseURL = import.meta.env.VITE_OMDB_API_BASE_URL,
  apikey = import.meta.env.VITE_OMDB_API_APIKEY;
