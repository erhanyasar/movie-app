export type GlobalState = {
  movies: {
    data: {
      searchResult: Movie[];
      paginatedSearch: Movie[];
      selectedMovie: Movie;
      searchParam: string;
      searchYear: string;
      searchType: string;
    };
    isLoading: boolean;
    hasError: boolean;
    errorMessage: string;
  };
};

export type Movie = {
  Actors?: string;
  Awards?: string;
  Country?: string;
  Director?: string;
  Genre?: string;
  Language?: string;
  Metascore?: string;
  Plot?: string;
  Poster?: string;
  Rated?: string;
  Ratings?: Rating[];
  Released?: string | Date;
  Response?: string | boolean;
  Runtime?: string;
  Title: string;
  Type?: string;
  Writer?: string;
  Year: string | number;
  imdbID: string;
  imdbRating?: string | number;
  imdbVotes?: string | number;
  totalSeasons?: string | number;
};

export type Rating = {
  Source: string;
  Value: string | number;
};
