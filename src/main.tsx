import { StrictMode, Suspense } from "react";
import ReactDOM from "react-dom/client";
import {
  createTheme,
  ThemeProvider,
  CircularProgress,
  CssBaseline,
} from "@mui/material";
import { Provider as ReduxProvider } from "react-redux";
import { store } from "./app/store";
import App from "./app/App.tsx";

const theme = createTheme();

ReactDOM.createRoot(document.getElementById("root")!).render(
  <StrictMode>
    <Suspense fallback={<CircularProgress />}>
      <ThemeProvider theme={theme}>
        <ReduxProvider store={store}>
          <CssBaseline />
          <App />
        </ReduxProvider>
      </ThemeProvider>
    </Suspense>
  </StrictMode>
);
