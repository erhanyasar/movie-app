import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { Card, Button, Typography, CircularProgress } from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import ReplyIcon from "@mui/icons-material/Reply";
import {
  setIsLoading,
  setHasError,
  selectMovie,
} from "../movieWrapper/movieWrapperSlice";
import { baseURL, apikey } from "../assets/constants";
import { GlobalState } from "../assets/types";

export default function MovieDetails(): JSX.Element {
  const { isLoading, hasError, data } = useSelector(
    (state: GlobalState) => state.movies
  );
  const dispatch = useDispatch();
  const { id } = useParams();

  useEffect(() => {
    const url = `${baseURL}?i=${id}&apikey=${apikey}`;

    const fetchMovie = async () => {
      await fetch(url)
        .then((response) => response.json())
        .then((response) => dispatch(selectMovie(response)));
    };

    try {
      fetchMovie();
    } catch (err) {
      console.error(err);
      dispatch(setHasError(true));
    } finally {
      dispatch(setIsLoading(false));
    }
  }, [id, dispatch]);

  return (
    <Grid container spacing={2} mt={1}>
      <Grid xs={5} my={1}>
        <Button
          variant="contained"
          startIcon={<ReplyIcon />}
          onClick={() => history.back()}
        >
          Return Home
        </Button>
      </Grid>
      <Grid xsOffset={4} xs={5}>
        <Card>
          {isLoading && <CircularProgress />}
          {hasError && <>There seems to be an error, please refresh the page</>}
          {data &&
            Object.entries(data.selectedMovie).map((entry, index) => {
              return (
                <div key={index} style={{ padding: "10px" }}>
                  <Typography variant="h6" component="h2" color="primary">
                    {entry[0]}
                  </Typography>
                  <Typography variant="body1" component="span">
                    {typeof entry[1] === "string"
                      ? entry[1]
                      : entry[1].toString()}
                  </Typography>
                </div>
              );
            })}
        </Card>
      </Grid>
    </Grid>
  );
}
