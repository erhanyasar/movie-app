import { useState } from "react";
import {
  Card,
  CardContent,
  Button,
  Typography,
  Stack,
  Grid,
} from "@mui/material";
import { MoviesPagination } from "./moviesPagination";
import { Movie, GlobalState } from "../../assets/types";
import { useSelector } from "react-redux";

export const MoviesList = ({
  moviesList,
  errorMessage,
}: {
  moviesList: Movie[];
  errorMessage: string;
}): JSX.Element => {
  const { data } = useSelector((state: GlobalState) => state.movies);

  const [paginatedMoviesList, setPaginatedMoviesList] = useState(
    data.paginatedSearch
  );

  return (
    <Grid container spacing={2}>
      {errorMessage && <>{`Search Error: ${errorMessage}`}</>}
      {!errorMessage &&
        paginatedMoviesList &&
        paginatedMoviesList.map((movie) => (
          <Grid item key={movie.imdbID} sm={12} md={6} flexWrap="wrap">
            <Card>
              <CardContent>
                <Grid container>
                  <Grid item sm={4} md={6}>
                    <img
                      src={movie.Poster}
                      alt={`${movie.Title} - ${movie.Year} - ${movie.imdbID}`}
                      width="200"
                      height="250"
                    ></img>
                  </Grid>
                  <Grid item sm={8} md={6}>
                    <Typography variant="h6" component="h2" color="primary">
                      Title
                    </Typography>
                    <Typography variant="body1" component="span">
                      {movie.Title}
                    </Typography>
                    <Typography variant="h6" component="h2" color="primary">
                      Released
                    </Typography>
                    <Typography variant="body1" component="span">
                      {movie.Year}
                    </Typography>
                    <Typography variant="h6" component="h2" color="primary">
                      IMDB ID
                    </Typography>
                    <Typography variant="body1" component="span">
                      {movie.imdbID}
                    </Typography>
                    <Stack direction="column" mt={5}>
                      <Button
                        size="small"
                        variant="outlined"
                        onClick={() =>
                          location.assign(`/movie-details/${movie.imdbID}`)
                        }
                      >
                        Learn More
                      </Button>
                    </Stack>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          </Grid>
        ))}
      {!errorMessage && moviesList && (
        <MoviesPagination setMovies={setPaginatedMoviesList} />
      )}
    </Grid>
  );
};
