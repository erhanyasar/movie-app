import {
  useState,
  useEffect,
  ChangeEvent,
  Dispatch,
  SetStateAction,
} from "react";
import { useSelector, useDispatch } from "react-redux";
import { Stack, Grid, Pagination } from "@mui/material";
import { Movie, GlobalState } from "../../assets/types";
import { savePagination } from "../movieWrapperSlice";

const pageSize = 10;

export const MoviesPagination = ({
  setMovies,
}: {
  setMovies: Dispatch<SetStateAction<Movie[]>>;
}) => {
  const { data } = useSelector((state: GlobalState) => state.movies);
  const dispatch = useDispatch();

  const [pagination, setPagination] = useState({
    count: 0,
    from: 0,
    to: pageSize,
  });

  const handlePageChange = (_e: ChangeEvent<unknown>, page: number) => {
    setPagination({
      ...pagination,
      from: (page - 1) * pageSize,
      to: (page - 1) * pageSize + pageSize,
    });
  };

  useEffect(() => {
    setPagination({
      ...pagination,
      count: data.searchResult.length,
    });
    setMovies(data.searchResult.slice(pagination.from, pagination.to));
    dispatch(
      savePagination(data.searchResult.slice(pagination.from, pagination.to))
    );
  }, [data.searchResult, pagination.from, pagination.to]);

  return (
    <Grid item sm={12} className="pagination-outer-wrapper">
      <Stack spacing={2} className="pagination-inner-wrapper">
        <Pagination
          count={Math.ceil(pagination.count / pageSize)}
          color="primary"
          onChange={handlePageChange}
        />
      </Stack>
    </Grid>
  );
};
