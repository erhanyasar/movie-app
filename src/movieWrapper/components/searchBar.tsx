import { ChangeEventHandler, MouseEventHandler } from "react";
import {
  Button,
  TextField,
  Grid,
  FormControl,
  FormControlLabel,
  FormLabel,
  RadioGroup,
  Radio,
  Stack,
  Typography,
} from "@mui/material";

export const SearchBar = ({
  searchParam,
  searchYear,
  searchType,
  onSearchParamChange,
  onSearchYearChange,
  onRadioChange,
  onInputSubmit,
}: {
  searchParam: string;
  searchYear: string;
  searchType: string;
  onSearchParamChange: ChangeEventHandler<HTMLInputElement>;
  onSearchYearChange: ChangeEventHandler<HTMLInputElement>;
  onRadioChange: ChangeEventHandler;
  onInputSubmit: MouseEventHandler<HTMLButtonElement>;
}): JSX.Element => {
  return (
    <Grid container direction="row" spacing={5} mt={1} mb={5}>
      <Grid item xs={9}>
        <Typography variant="h5" component="span" color="secondary">
          Input Search Parameters
        </Typography>
      </Grid>
      <Grid item xs={9}>
        <Stack direction="column" className="input-text-fields">
          <TextField
            value={searchParam}
            label="Enter search params"
            variant="standard"
            onChange={onSearchParamChange}
          />
          <TextField
            value={searchYear}
            label="Enter release year"
            variant="standard"
            onChange={onSearchYearChange}
            type="number"
          />
        </Stack>
      </Grid>
      <Grid item xs={3} className="radio-group-controller">
        <FormControl>
          <FormLabel id="demo-controlled-radio-buttons-group">
            Search Type
          </FormLabel>
          <RadioGroup
            aria-labelledby="demo-controlled-radio-buttons-group"
            name="controlled-radio-buttons-group"
            value={searchType}
            onChange={onRadioChange}
          >
            <FormControlLabel value="movie" control={<Radio />} label="Movie" />
            <FormControlLabel
              value="series"
              control={<Radio />}
              label="Series"
            />
            <FormControlLabel
              value="episode"
              control={<Radio />}
              label="Episode"
            />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <Stack direction="column">
          <Button variant="contained" onClick={onInputSubmit}>
            Submit Search
          </Button>
        </Stack>
      </Grid>
    </Grid>
  );
};
