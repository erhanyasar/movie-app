import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  data: {
    searchResult: [],
    paginatedSearch: [],
    selectedMovie: {},
    searchParam: "",
    searchYear: "",
    searchType: "",
  },
  isLoading: true,
  hasError: false,
  errorMessage: "",
};

const movieSlice = createSlice({
  name: "movie",
  initialState,
  reducers: {
    setIsLoading: (state, action) => {
      return (state = {
        ...state,
        isLoading: action.payload,
      });
    },
    setHasError: (state, action) => {
      return (state = {
        ...state,
        hasError: action.payload,
      });
    },
    setErrorMessage: (state, action) => {
      return (state = {
        ...state,
        errorMessage: action.payload,
      });
    },
    saveMovies: (state, action) => {
      return (state = {
        ...state,
        data: {
          ...state.data,
          searchResult: action.payload,
        },
      });
    },
    selectMovie: (state, action) => {
      return (state = {
        ...state,
        data: {
          ...state.data,
          selectedMovie: action.payload,
        },
      });
    },
    saveSearchParam: (state, action) => {
      return (state = {
        ...state,
        data: {
          ...state.data,
          searchParam: action.payload,
        },
      });
    },
    saveSearchYear: (state, action) => {
      return (state = {
        ...state,
        data: {
          ...state.data,
          searchYear: action.payload,
        },
      });
    },
    saveSearchType: (state, action) => {
      return (state = {
        ...state,
        data: {
          ...state.data,
          searchType: action.payload,
        },
      });
    },
    savePagination: (state, action) => {
      return (state = {
        ...state,
        data: {
          ...state.data,
          paginatedSearch: action.payload,
        },
      });
    },
  },
});

export const {
  setIsLoading,
  setHasError,
  setErrorMessage,
  saveMovies,
  selectMovie,
  saveSearchParam,
  saveSearchYear,
  saveSearchType,
  savePagination,
} = movieSlice.actions;
export default movieSlice.reducer;
