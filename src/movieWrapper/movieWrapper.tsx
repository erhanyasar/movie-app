import { useEffect, useState, ChangeEvent } from "react";
import { useSelector, useDispatch } from "react-redux";
import { CircularProgress, Grid } from "@mui/material";
import {
  setIsLoading,
  setHasError,
  setErrorMessage,
  saveMovies,
  saveSearchParam,
  saveSearchYear,
  saveSearchType,
} from "./movieWrapperSlice";
import { GlobalState } from "../assets/types";
import { SearchBar } from "./components/searchBar";
import { MoviesList } from "./components/moviesList";
import { baseURL, apikey } from "../assets/constants";

export default function MovieWrapper(): JSX.Element {
  const { isLoading, hasError, errorMessage, data } = useSelector(
    (state: GlobalState) => state.movies
  );
  const dispatch = useDispatch();

  const [searchParameter, setSearchParameter] = useState(data.searchParam);
  const [searchYear, setSearchYear] = useState(data.searchYear);
  const [searchType, setSearchType] = useState(data.searchType);
  const [urlParams, setUrlParams] = useState("?s=pokemon");

  const handleSearchParamChange = (e: ChangeEvent<HTMLInputElement>): void => {
    setSearchParameter(e.target.value);
    dispatch(saveSearchParam(e.target.value));
  };

  const handleSearchYearChange = (e: ChangeEvent<HTMLInputElement>): void => {
    if (Number(e.target.value)) {
      setSearchYear(e.target.value);
      dispatch(saveSearchYear(e.target.value));
    } else alert("Please provide input as a number");
  };

  const handleRadioChange = (e: ChangeEvent<HTMLInputElement>): void => {
    setSearchType(e.target.value);
    dispatch(saveSearchType(e.target.value));
  };

  const handleInputSubmit = (): void => {
    if (data.searchParam) setUrlParams(`?s=${data.searchParam}`);
    if (data.searchYear)
      setUrlParams((urlParams) => `${urlParams}&y=${data.searchYear}`);
    if (data.searchType)
      setUrlParams((urlParams) => `${urlParams}&type=${data.searchType}`);
  };

  useEffect(() => {
    const url = `${baseURL}${urlParams}&apikey=${apikey}`;

    const fetchMovies = async () => {
      await fetch(url)
        .then((response) => response.json())
        .then((response) => {
          if (response.Response === "True") {
            dispatch(setErrorMessage(""));
            dispatch(saveMovies(response.Search));
          } else if (
            response.Response === "False" ||
            Object.prototype.hasOwnProperty.call(response, "Error")
          )
            dispatch(setErrorMessage(response.Error));
        })
        .catch((error) => dispatch(setErrorMessage(error)));
    };

    try {
      fetchMovies();
    } catch (err) {
      console.error(err);
      dispatch(setHasError(true));
    } finally {
      dispatch(setIsLoading(false));
    }
  }, [urlParams, dispatch]);

  if (isLoading) return <CircularProgress />;
  if (hasError) return <>There seems to be an error, please refresh the page</>;
  if (!data) return <></>;

  return (
    <Grid container spacing={5}>
      <Grid item xs={12}>
        <SearchBar
          searchParam={searchParameter}
          searchYear={searchYear}
          searchType={searchType}
          onSearchParamChange={handleSearchParamChange}
          onSearchYearChange={handleSearchYearChange}
          onRadioChange={handleRadioChange}
          onInputSubmit={handleInputSubmit}
        />
        <MoviesList
          moviesList={data.searchResult}
          errorMessage={errorMessage}
        />
      </Grid>
    </Grid>
  );
}
